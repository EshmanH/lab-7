public class Board {
    private Square[][] tictactoeBoard;

    // Constructor, initialize the Square[][] field to a 3 x 3 array
    public Board() {
        tictactoeBoard = new Square[3][3];
        for (int i = 0; i < tictactoeBoard.length; i++) {
            for (int j = 0; j < tictactoeBoard.length; j++) {
                tictactoeBoard[i][j] = Square.BLANK;  //initialize to blank
            }
        }
    }

    // Override toString() method
    public String toString() {
        String grid = "";
        for (int i = 0; i < tictactoeBoard.length; i++) {
            for (int j = 0; j < tictactoeBoard.length; j++) {
                grid += tictactoeBoard[i][j] + " ";
            }
            grid += "\n";
        }
        return grid;
    }

    // placeToken() method
    public boolean placeToken(int row, int col, Square playerToken) {
        if ((row >= 0 && row < 3) && (col >= 0 && col < 3)) {
            if (tictactoeBoard[row][col] == Square.BLANK) {
                tictactoeBoard[row][col] = playerToken;
                return true;
            }
        }
        return false;
    }

    // checkIfFull() method
    public boolean checkIfFull() {
        for (int i = 0; i < tictactoeBoard.length; i++) {
            for (int j = 0; j < tictactoeBoard.length; j++) {
                if (tictactoeBoard[i][j] == Square.BLANK) {
                    return false;
                }
            }
        }
        return true;
    }

    // checkIfWinningHorizontal() method
    private boolean checkIfWinningHorizontal(Square playerToken) {
        for (int i = 0; i < tictactoeBoard.length; i++) {
            if (tictactoeBoard[i][0] == playerToken && tictactoeBoard[i][1] == playerToken && tictactoeBoard[i][2] == playerToken) {
                return true;
            }
        }
        return false;
    }

    // checkIfWinningVertical() method
    private boolean checkIfWinningVertical(Square playerToken) {
        for (int i = 0; i < tictactoeBoard.length; i++) {
            if (tictactoeBoard[0][i] == playerToken && tictactoeBoard[1][i] == playerToken && tictactoeBoard[2][i] == playerToken) {
                return true;
            }
        }
        return false;
    }

    // checkIfWinning() method
    public boolean checkIfWinning(Square playerToken) {
        if (checkIfWinningHorizontal(playerToken) || checkIfWinningVertical(playerToken)) {
            return true;
        }
        return false;
    }
}
