import java.util.Scanner;

public class TicTacToeGame {
    
    public static void main(String[] args) {
        //1. print messages
        System.out.println("Welcome to my Tic Tac Toe game");
        System.out.println("Player 1's token: X");
        System.out.println("Player 2's token: O");

        //2. initialize board
        Board board = new Board();
        //3. variable declarations gameOver(Boolean FALSE), player(1), playerToken(player Symbol)
        boolean gameOver = false;
        int player = 1;
        Square playerToken = Square.X;
        //4.
        while(gameOver==false) {
            System.out.println(board); //Print board object
            
            //check which player turn it is, and change token accordingly.
            if(player == 1) {
                playerToken = Square.X;
            } 
            else {
                playerToken = Square.O;
            }
            
            //2 integer inputs from user, row and column number 0,1,2
            Scanner scanner = new Scanner(System.in);
            System.out.print("Player " + player + ", please enter row and column to place your token \n");
            int row = scanner.nextInt();
            int col = scanner.nextInt();
            //while loops to ask user to re-input row and column number if they enter an invalid value
            while(board.placeToken(row, col, playerToken)==false) {
                System.out.println("Invalid input, please enter row and column again: ");
                row = scanner.nextInt();
                col = scanner.nextInt();
            }
            //check if board is full and prints tie if it is
            if(board.checkIfFull()) {
                System.out.println("It's a tie!");
                gameOver = true;
            } 
            //check if there is a winner and prints the player who won
            else if(board.checkIfWinning(playerToken)) {
                System.out.println(board);
                System.out.println("Player " + player + " is the winner!");
                gameOver = true;
            } 
            //player change 
            else {
                player++;
                if(player > 2) {
                    player = 1;
                }
            }
        }
        
    }
}
