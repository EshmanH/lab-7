enum Square {

    BLANK("_"), X("X"), O("O");
    private String symbol;
    
    Square(String symbol) {
        this.symbol = symbol;
    }
    
    public String toString() {
        return symbol;
    }
}
